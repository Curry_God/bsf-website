---
title: "Workout nutrition"
output:
  html_document:
    toc: true
---

# Macro's en Micro's

Your caloric budget should be spend to get the following micronutients. Calculate your macro's and micro's and create some meals.

## Protein

Try to shoot for 0.25g-0.40g of protein/kg of bodyweight in each meal (4-7x a day)
High quallity Protein Sources:

* 90% Lean ground beef
* Chicken breast
* Other lean cuts of red and white meat.
* Whey Isolate
* Eggs or egg whites
* white and fat Fish
* Soft cheeses like cottage cheese, greek yoghurt
* Fat free milk (or lactose free for and even better protein ratio)

## Fats

* High omega 3 fish (salmon, sardines, mackerel, cod liver).
* Nuts (Brazil nuts, almonds, cashews, walnuts)- omega 6 primarily
* Nut butters (Peanut butter, almond butter)- omega 6 primarily
* Seeds (flaxseed, chaiseed) omega 3 and omega 6
* Oils such (Olive oil and coconut oil)
* Grassfed butter

## Carbs

* Granola
* White/brown rice
* White/brown pasta
* Wholegrain bread
* Berries, high in antioxidants and vitamins/minerals
* Veggies, high in antioxidants and vitamins/minerals
* Fruits

## Fiber

Fibre can help with digestion, and can be manipulated to get delivery of nutrients later rather than sooner.
Foods high in fibre include; vegetables, dark grains and carbohydrates such as brown rice, brown pasta, wholemeal breads.
Diets high in fibre may also help decrease your risk of colon cancers

# Nutrition for workouts

It is important to consider workout nutrition if you would like to optimise performance, recovery and muscle building. 

* Eat to fuel your growth, if you're hungry eat, if you're not hungry wait for your body to tell you to eat. 
* Have 4+ servings of 20-30g protein a day this will optimise muscle building. As a rule of thumb each serving of protein should be 0.25g-0.4g/kg of bodyweight. 
* If you're a 100kg person you'll need 25g-40g protein in a meal to help spike MPS. Anymore than this will see no added benefit typically. Wait 2-3 hours before another serving. 

* Consider pre, intra and post workout nutrition. Eat lots of fast digesting carbohydrates pre and post workout to spike insulin levels for training, and replenish glycogen stores post workout. 
* Try not to eat too much high fibre/high food around your workout window as this will delay digestion of faster digesting carbs for optimal performance in the gym. 

## Pre workout nutrition

* Ideally pre workout you should consume foods high in fast disgesting carbohydrates (foods high in carbs, but low in fibre)

* Eat foods low in fat, fats slow down digestion which is not what you want before a workout as fast digesting carbohydrates are going to be the main source of energy in resistance training. 

* Hydrate

## Intraworkout nutrition

* Optimally it would be good to get in a highly branched cyclic dextrin during a workout, along with EAAs. Both digest extremely fast which is great for when you need fuel during a long hard session. 

* Cyclic dextrin is a low molecular weight carbohydrate that causes little processing by your gastrointestinal track, only down side to this is the expense of the supplement. 

* Overall intraworkout nutrition has a small benefit and is probably something you should consider doing if you"'"re looking to be as efficient as you can (i.e looking to compete in bodybuilding). For the average gym goer it is not very important

## Postworkout nutrition

* Post workout nutrition is extremely important as it has an effect on your recovery. If you can recovery faster you can deal with more overall volume, which can lead to more muscle growth.

* Good postworkout nutrition would be similar to pre workout nutrition (fast digesting carbs, low fibre, and low fat). The reason for this is because glut-4 receptors are upregulated post workout therefore your ability to absorb carbohydrates is a lot higher so its a good opportunity to intake faster digesting carbs. 

* Post workout it is a good idea to relax, and get into a parasympathetic state. Protein consumption post workout is thought to be important, which it is but that doesn"'"t mean you have to chug down a protein shake right after.

* Take your time with ingestion of protein after a workout, it isn"'"t essential. 

* Hydrate

## Preworkout 

* Pre-workout can help increase focus during a workout. However, there"'"s lack of evidence suggesting that there is any performance benefits till the individual takes 4mg/kg caffeine per kg bodyweight. 

* Therefore It is suggested that you only take preworkout when you really need it, i.e you"'"re feeling very tired... Or just have a coffee.

## When should you be ingesting preworkout? 

* Since caffeine stays in your system for 10-12 hours it is highly recommended that you use it as early as possible in your day, especially at higher dosages.

* Caffeine will block deep sleep, consequently causing worse muscle recovery, and giving less gains as a result. This is also not optimal for health as deep sleep is needed in order to support development and fight off disease. 

* Therefore use preworkout cautiously if you want to optimise health and muscle growth. 

## Calculating your macronutrients 

* Once you understand exactly how many calories you need to achieve your goal you will need to need to figure what macronutrients those calories will compose of. 

* Typically carbohydrates and proteins will be calculated first. If you"'"re in a muscle building phase aim for 4-6g/kg bodyweight of carbohydrates. We will use 5g/kg bodyweight. This would mean if you are 100kg you will need 500g carbohydrates. 1g carb = 4 calories, therefore 500*4=2000 calories. 

* Next protein will be calculated, aim for around 2g/kg bodyweight. Therefore, if you are 100kg you will need 200g total protein. 1g of protein = 4 calories, therefore 200*4=800 calories. 

* Lastly fat should be calculated. For example sake this 100kg person needs 4,000 calories per day to gain weight at a rate of 0.5kg/week. (Protein= 800 calories and carbohydrate =2,000 calories. This makes 2,800 calories total between carbs and protein.)

* We know that this 100kg person needs 4,000 calories consequently the rest will be filled with fat. 4,000-2,800=1,200. 1g fat = 9 calories, 1200/9=133g. Therefore, this leaves us with 500g C, 200g P, 133g F. 

* Here the fat could be taken down even further, as from scientific literature we understand that more carbohydrates is typically better for recovery and performance outcomes versus fat. Typically you could get away with only having 75-100g fat. So therefore, more could be taken off of the fat about -30g. 30g fat would equal 270 calories (30*9). These 270 calories can be given to carbohydrate giving us around 70g extra carbohydrates (270/4=68). 

* Overall this would take us to 570g carbohydrate, 100g fat, 200g protein and 4000 calories. 

* Finally fibre intake should also be considered as this is important for gut health, the motoility of feaces and reduces the risk of diseases such as cancer. The recommendation for tihs would be 30g/day. 
