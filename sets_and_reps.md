---
title: "Rep ranges and building sets"
output:
  html_document:
    toc: true
---

# How many Sets?

Renaissance Periodisations take on different levels of volume is a pretty good place to start ([Training Volume Landmarks for Muscle Growth](https://renaissanceperiodization.com/training-volume-landmarks-muscle-growth/)). Another good chart is the [prilepins chart](https://fitatmidlife.com/prilepins-chart-explained/), which is more strength orientated. To make everything super simple you can simply follow the chart bellow on how many sets per muscle group you need per week. It's not perfect but it should help you get started. It slows how many sets an average beginner / intermediate lifter should do on most split programs.

```
Muscle      Sets per week
—————————————————————————
Chest       9-12
Back        9-12
Shoulders   6-12
Triceps     6-12
Biceps      6-12
Abs         0-9
Quads       9-12
Hamstrings  9-12
Calves      6-12
```

After you've set up your program with what you think would be a decent amount of sets per week. Simply test your program for a few months. If you never feel sore and fatiqued from your workouts it might mean you can do more sets than you currently do. Add a few sets, test your program, and see what works for you. There is not a perfect awnser to "how many sets should I do?". Try it, if it works it works. If it doesn't, change it.
Here are some indicators that you might need to change your volume:

* Are you currently growing? Consider not changing anything.
* Are you anxieus that sets earlier in the session will hinder your later sets? Consider doing less sets.
* If you never get a pump, consider adding more sets.
* If you never get sore, consider adding more sets.
* Are you recovered enough for your next session? If yes, maybe add more, if no, consider doing less.

# Rep ranges

There is been a lot of debate about rep ranges, how to progress them, if you should mix them up etc.
I will talk about these points in this section. There is a commen saying that training for 8-12 reps makes your muscles grow bigger, while 3-6 reps make your muscle grow stronger. This is a very abstracted beginner friendly way of representing rep ranges. In truth, the higher on the rep range you go, the more volume you get, while generating less fatique. If you want to grow your chest you will want as much volume as possible with the least amount of fatique, given that the rep is taken to an effective intensity (usually 0-2 RIR). If you want to get 25 total reps you can either do 3x8 or do 8x3 as an example. The 3 sets of 8 will be much less fatiuqing than the 8 sets of 3. If you are a bodybuilder you will want less fatique for more volume, while if  you're training for strength you will want more intensity to get stronger. In the end if you manage to get the same amount of volume for a certain lift, you will grow equally as well, it is just that if you do 8 sets of 3, you won't have any energy left to do a lot of other sets in your workouts. This is why bodybuilders choose to work in the higer rep ranges. The ranges bellow are very abstracted so they can be implemented in a bodybuilder and strength program. But the hypertrophy rep range is anywhere from 1-30 reppetitions in truth. All rep ranges have their use cases for both size and strength.
As a beginner however you will want to train in the lower rep ranges (4-6) since training this low also helps you get started with form. Since you're a beginner you also won't generate enough fatique to burn yourself out with lower reppetitions.

```
Goal                             Rep range
——————————————————————————————————————————
Sarcoplasmic hypetrophy           10-20
Myofibrillair hypetrophy          1-3
Abstracted range for hypetrophy   8-12
Abstracted range for strength     3-8     
```

# Creating sets

Next up we need to talk about how you actually can build up different types of sets, and why you would want to use these types of sets.

**Straight sets** Straight sets are usually the go too, standaard type of set you do when you train with bodybuilding and strength sports. You usually see them define as 3x8 for example, which means 3 sets of 8 reppetitions. When you can preform 8 sets accross all 3 sets you up the weight by usually 1-2% of  your working weight. The advantage of these types of sets is that you can train at different types of intensities with the same weight. The first set may be an RPE 7, while the last set may be an RPE 10. The disadvantage is that you may not train in the intensity you want. Your first set can also be an RPE5. If you you may end with an RPE of 11 and only get 7 reps for the last set. If you want to stay in a specific RPE like 7-9 for a more effective stimulus to fatique ratio you may need to try a different set technique.

```
Example Straight sets:
8 seps at @ 100kg
8 seps at @ 100kg
7 seps at @ 100kg <-- missed last rep

Example notation:
Squats 3x8
```

**Top sets and back off sets** As you become more advanced you need to mix up your rep ranges more and more. As a bodybuilder you still want to get stronger, since more strength potential means more size potential. The same hold true for powerlifters, if you want to get stronger you need to get bigger. This is why as an intermediate / advanced trainee you need to mix rep ranges and intensities. A very simply way to do this is to do a top and back of set in your session. For bodybuilder a top set would be one set of 6-9 reps. After you did your top set you would do another few back off sets for the same movement in the 9-13 rep range for example. As a strength trainee you usually do a top set in the 3-6 rep range, while the back off sets may be sets of 3-6 with simply less weight, or reps of 5-10. Some trainee's prefer to have these top and back of sets on the same days, but you can also split them up into different training days. There will be example further in this guide about how to program top and back of sets. The idea of these top and back of sets is that you will want to train at different intensities at the end of the day, no matter how you choose to do this. The RPE for the top and back of sets can be more effective this way. Your top set can be in a specific intensity, say RPE8, while your back off sets can also be an RPE7-9.

```
Example top set back of sets:
8 reps @ 100kg
12 reps @ 85kg
12 reps @ 85kg

Example notation:
Squats 1x8, 2x12
```

**Ascending Pyramid sets** These type of sets are basically straight sets where the intensity accross all sets is made to be fairly the same. We start the first set with a higher rep range than that we will end. This ensures that the RPE accoross all sets can be at least fairly simulair. If we program them perfectly we can end up with all sets having the same RPE, say an RPE of 8.

```
Example Ascending Pyramid sets:
10 reps @ 100kg
9 reps @ 100kg
8 reps @ 100kg

Example notation:
Squats 1x10,1x9,1x8
```

**Reverse Pyramid Sets** Pyramid sets are basically top and back off sets, but instead of having one top set and 2-3 back of sets you keep removing weight off the bar eatch set. This way we can also regulate the RPE of eatch set.
```
Example Reverse Pyramid Sets:
5 reps @ 100kg
10 reps @ 82kg
15 reps @ 70kg

Example Notation:
Squats 1x5, 1x10, 1x15
```

**Evolving rep ranges** You can also train in a specfic rep range instead of defining them like with ascending pyramid sets. We simply define a 3x8-12 which gives the trainee the freedom to get any rep between 8 and 12. This way we can autoregulate the training intensity for a set. Say we only want sets with an RPE of 9, we can simply hit and RPE of 9, and the reps shouldn't matter for when we stop the set.

```
Example training in a rep range:
10 reps @ 100kg @ RPE 9
9 reps @ 100kg @ RPE 9
8 reps @ 100kg @ RPE 9

Example Notation:
Squats 3x8-12 @ RPE 9
```

**Load Drop Method, fatique percentages**
Fatique percentages are probably one of the more fancy tricks on this webpage. It is a way to autoregulate the amount of sets you do. You may do 5 sets on a good day and 1 on a bad day. This system is designed by Mike Tuchscherer. It is a pretty difficult system since you need to be very good at estemating RPE.
We start with a top set of 100kg for 5’s at an RPE of 9. We want to create 5% fatique. First we take 5% off the bar, and than we do another set. This set might be any RPE, in the example bellow it is an RPE of 8. We keep doing sets untill we get an RPE of 9 again. This is called the Load Drop Method.

```
Example drop load percentages:
5 reps @ 100kg @ RPE9
drop 5% and do sets of 5 until you hit another RPE 9 set
5 reps @ 95kg @ RPE8
5 reps @ 95kg @ RPE9
^ RPE is the same as top set, we now have 5% fatique.

Notation example:
Squats 8-12 drop load untill 5-9% fatique @ RPE9
```

**Repeat method, fatique percentages**

We also have a repeats method fatique percentages. Instead of droping a load we pick a starting weight at a certain RPE. The amount the RPE increases in back of sets determines how much fatique we’ve created. We need a simple chart for this. An example and the chart are shown bellow. In the example bellow we want to generate 4-6% fatique.

```
RPE increse   Fatique generated
________________________________
0.5           2-3%
1             4-6%
1.5           6-9%
2             9-12%
```

```
Example repeat sets:
100x3@8
100x3@8.5
100x3@8.5
100x3@9, 4-6% fatigue, we stop here

Notation example:
Squats 8-12 repeat untill 5-9% fatique @RPE8
```

**Plain repeats**

Fatique percentages can be a bit arbitrary especially if you don't use the drop loads and repeats together. We can simply run repeat sets with in increase in RPE without specifying a goal percentage. The easiest way to implement this method if you are bad at guessing your RPE is by running something like the 2th example. Here we simply do a set to failure untill we have dropped a rep on one of the sets.

```
Example repeat sets:
100x3@8
100x3@8.5
100x3@8.5
100x3@9, +1RPE / -1 rep.

Example 2 repeat sets:
100x8@9.5
100x8@10
100x7@11 +1RPE / -1 rep.

Notation:
Ez curls 8-12, repeat sets to +1RPE, @RPE10
```
