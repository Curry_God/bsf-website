---
title: "BSF fitness FAQ"
output:
  html_document:
    toc: true
---

# Do fatburners work?

Depends what is in them but in general no they are not effective.
They might have caffeine in there which helps with appetite. But to put it bluntly, its not worth your money and the results are very very minimal at best. Usually these supplements are scams and don't do anything. The best way to lose fat is to simply eat less callories.

# How many sets should I do?

[This many](https://clemens0.gitlab.io/bsf-website/clemens_split_guide.html#volume)
